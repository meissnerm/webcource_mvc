package de.meissner.webcourse.controller.Student;

import de.meissner.webcourse.model.Student;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;


@Controller
@EnableWebMvc
@RequestMapping("/Admission")
public class StudentController
{
    //spring MVC-tutorial kap15 & 16
    @InitBinder
    public void initBinding(WebDataBinder binder){
        binder.setDisallowedFields(new String[] {"mobile"});

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy****MM****dd");
        binder.registerCustomEditor(Date.class, "dateOfBirth", new CustomDateEditor(dateFormat, false));

        binder.registerCustomEditor(String.class, "name", new StudentNameEditor());
    }

    //alle models in diesem Controller haben die zugewiesenenen Objekte
    //wann immer eine solche Methode in einem Controller existiert
    //Dann führt MVC immer erst diese Methode aus, bevor die Requestmethoden ausgeführt werden
    @ModelAttribute
    public void addingCommonObjects(Model model) {
        model.addAttribute("headerMessage", "Geben sie ihren Text ein");
    }

    @RequestMapping("")
    public ModelAndView index() {
        ModelAndView model = new ModelAndView("/Admission/Admission");
        return model;
    }

    @RequestMapping(value = "/Submit", method = RequestMethod.POST)
    public ModelAndView submitAdmissionForm(@Valid @ModelAttribute("student") Student student, BindingResult result) {

        ModelAndView model = returnModelViewDependingOnError(result,"/Admission/AdmissionSuccess", "/Admission/Admission" );
        model.addObject("student", student);
        return model;
    }

    private ModelAndView returnModelViewDependingOnError(BindingResult result, String viewName, String viewNameOnError){
        if (result.hasErrors()){
            return new ModelAndView(viewNameOnError);
        }
        return new ModelAndView(viewName);
    }



 /*   @RequestMapping(value = "/Submit", method = RequestMethod.POST)
    public ModelAndView submitAdmissionForm(@RequestParam(value = "studentName", defaultValue = "keine Angabe") String name,
                                            @RequestParam("studentHobby") String hobby) {

        Student student1 = new Student();
        student1.setStudentName(name);
        student1.setStudentHobby(hobby);

        ModelAndView model = new ModelAndView("/Admission/AdmissionSuccess");
        model.addObject("msg", "Details submitted by you:: name:" + name + ", Hobby: " + hobby);
        model.addObject("student1", student1);
        return model;
    }*/


  /*  @RequestMapping(value = "/Submit", method = RequestMethod.POST)
    public ModelAndView submitAdmissionForm(@RequestParam Map<String,String> reqParams) {
        ModelAndView model = new ModelAndView("/Admission/AdmissionSuccess");
        model.addObject("msg", "Details submitted by you:: name:" + reqParams.get("studentName") + ", Hobby: " + reqParams.get("studentHobby"));
        return model;
    }*/
}