package de.meissner.webcourse.configuration;

import de.meissner.webcourse.controller.Student.DayOfWeekBasedAccessInterceptor;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
public class MVCConfiguration extends WebMvcConfigurerAdapter
{

    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/res/**").addResourceLocations("/static/");
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new DayOfWeekBasedAccessInterceptor());
    }



    @Bean()
    DispatcherServlet dispatcherServlet() {
        DispatcherServlet dispatcherServlet = new DispatcherServlet();
        return dispatcherServlet;
    }

    @Bean()
    ServletRegistrationBean dispatcherServletRegistration() {
        ServletRegistrationBean registration = new ServletRegistrationBean(dispatcherServlet(), "/");
        registration.setName("MVCDispatcher");
        return registration;
    }

    @Bean()
    ViewResolver viewResolver(){
        InternalResourceViewResolver irvr = new InternalResourceViewResolver();
        irvr.setPrefix("/WEB-INF/view/jsp/");
        irvr.setSuffix(".jsp");
        return irvr;
    }

    @Bean()
    MessageSource messageSource(){
        ReloadableResourceBundleMessageSource messSource = new ReloadableResourceBundleMessageSource();
        messSource.setBasename("/WEB-INF/MessageSources/studentmessages");
        messSource.setCacheSeconds(1);
        //wenn eine änderung in der properties-Datei stattfindet, wird das 1 sek danach von MVC ausgelesen
        //nur in entwicklungsversion nutzen nicht in Produktion
        return messSource;
    }

}
