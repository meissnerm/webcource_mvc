package de.meissner.webcourse.controller.Student;

import de.meissner.webcourse.controller.Student.HobbyValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({ FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = { HobbyValidator.class })
public @interface IsValidHobby {
    String message() default "Please provide a Valid hobby ";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };

    //durch das default ist der Parameter nun optional geworden
    //Jetzt muss der Annotation nicht zwingend dieser Parameter übergeben werden
    String listOfValidHobbies() default "Fotos";
}
