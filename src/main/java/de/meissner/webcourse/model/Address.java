package de.meissner.webcourse.model;

public class Address
{
    private String country;
    private String city;
    private String street;
    private String streetNr;
    private int postal;

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public String getStreet()
    {
        return street;
    }

    public void setStreet(String street)
    {
        this.street = street;
    }

    public String getStreetNr()
    {
        return streetNr;
    }

    public void setStreetNr(String streetNr)
    {
        this.streetNr = streetNr;
    }

    public int getPostal()
    {
        return postal;
    }

    public void setPostal(int postal)
    {
        this.postal = postal;
    }
}
