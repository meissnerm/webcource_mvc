<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: Meissner
  Date: 27.02.2018
  Time: 11:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admission</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>

<form:errors path="student.*" />

<form action="/Admission/Submit" method="post">
    <h1>${headerMessage}</h1>

    <table>
        <tr>
            <td>Student name:</td>
            <td><input type="text" name="name" value="${student.name}"/></td>
        </tr>
        <tr>
            <td>Student Hobby:</td>
            <td><input type="text" name="hobby"/></td>
        </tr>
        <tr>
            <td>Student Mobile:</td>
            <td><input type="text" name="mobile"/></td>
        </tr>
        <tr>
            <td>Student Birthday:</td>
            <td><input type="text" name="dateOfBirth"/></td>
        </tr>
        <tr>
            <td>Student Skills:</td>
            <td>
                <select name = "skills" multiple>
                    <option value="Java Core">Java Core</option>
                    <option value="Spring Core">SPRING CORE</option>
                    <option value="Spring MVC">Spring MVC</option>
                </select>
            </td>
        </tr>
    </table>

    <table>
        <tr>
            <td>Student Adresse:</td>
        </tr>
        <tr>
            <td>Country: <input type="text" name="address.country"/></td>
            <td>City: <input type="text" name="address.city"/></td>
            <td>Street: <input type="text" name="address.street"/></td>
            <td>Nr: <input type="text" name="address.streetNr"/></td>
            <td>Postal (int): <input type="text" name="address.postal"/></td>
        </tr>
    </table>

    <input type="submit" value="OK"/>
    <p>Get your Home <a href="/home/">here</a></p>
</form>

</body>
</html>
