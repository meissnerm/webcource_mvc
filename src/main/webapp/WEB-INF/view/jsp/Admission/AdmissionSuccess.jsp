<%--
  Created by IntelliJ IDEA.
  User: Meissner
  Date: 27.02.2018
  Time: 11:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>AdmissionSuccess</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
    <p>${msg}</p>
    <p>back <a href="/Admission/">here</a></p>

<table>
    <tr>
        <td>Student name:</td>
        <td>${student.name}</td>
    </tr>
    <tr>
        <td>Student Hobby:</td>
        <td>${student.hobby}</td>
    </tr>
    <tr>
        <td>Student Mobile:</td>
        <td>${student.mobile}</td>
    </tr>
    <tr>
        <td>Student Birthday:</td>
        <td>${student.dateOfBirth}</td>
    </tr>
    <tr>
        <td>Student Skills:</td>
        <td>${student.skills}</td>
    </tr>
    <tr>
        <td>Student Adress:</td>
        <td>Country: ${student.address.country}
        City: ${student.address.city}
        Street: ${student.address.street}
        Nr: ${student.address.streetNr}
        Postal (int): ${student.address.postal}</td>
    </tr>

</table>
</body>
</html>
