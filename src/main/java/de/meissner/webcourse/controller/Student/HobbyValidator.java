package de.meissner.webcourse.controller.Student;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class HobbyValidator implements ConstraintValidator<IsValidHobby, String> {

    private String listOfValidHobbies;

    @Override
    public void initialize(IsValidHobby isValidHobby) {
       this.listOfValidHobbies=  isValidHobby.listOfValidHobbies();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null){
            return false;
        }

        return value.matches(listOfValidHobbies);
    }
}
