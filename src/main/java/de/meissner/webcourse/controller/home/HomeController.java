package de.meissner.webcourse.controller.home;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.Map;


@Controller
@EnableWebMvc
@RequestMapping("/home")
public class HomeController
{
    @RequestMapping("/welcome")
    public ModelAndView welcome() {
        ModelAndView model = new ModelAndView("greeting");
        model.addObject("msg", "HelloWorld");
        return model;
    }

    @RequestMapping("/")
    public String greeting(@RequestParam(value="name", required=false, defaultValue="World") String name, Model model) {
        model.addAttribute("name", name);
        return "Home/index";
    }

    /*@RequestMapping("/test/{vorName}/{name}")
    public ModelAndView test(@PathVariable(required=false) String vorName, @PathVariable(required=false) String name) {
        ModelAndView model = new ModelAndView("test");
        model.addObject("name", name);
        model.addObject("vorName", vorName);
        return model;
    }
*/
    @RequestMapping(value = "/test/{vorName}/{name}")
    public ModelAndView test(@PathVariable Map<String,String> pathVars) {
        ModelAndView model = new ModelAndView("test");
        model.addObject("name", pathVars.get("name"));
        model.addObject("vorName", pathVars.get("vorName"));
        return model;
    }

    @RequestMapping("/welcomeRest")
    public ModelAndView welcomeRest()
    {
        ModelAndView mav = new ModelAndView("greeting");
        //mav.addObject("msg", new tester(2));
        return mav;
    }


}


