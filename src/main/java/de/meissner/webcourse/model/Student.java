package de.meissner.webcourse.model;

import de.meissner.webcourse.controller.Student.IsValidHobby;

import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.Date;

public class Student
{
    @Pattern(regexp = "[^0-9]*" )
    private String name;

    //@Size(min=2, max=10, message = "please enter a value for hobby between 2 an 10 characters")
    @Size(min=2, max=10 /*nun holt er sich die Message aus studentmessage.Properties, welches in der Bean messageSource definiert wurde*/)
    @IsValidHobby(listOfValidHobbies = "Music|Spielen")
    private String hobby;

    @Max(9999999)
    private Long mobile;

    @Past()
    private Date dateOfBirth;
    private ArrayList<String> skills;

    private Address address;

    public Address getAddress()
    {
        return address;
    }

    public void setAddress(Address address)
    {
        this.address = address;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getHobby()
    {
        return hobby;
    }

    public void setHobby(String hobby)
    {
        this.hobby = hobby;
    }

    public Long getMobile()
    {
        return mobile;
    }

    public void setMobile(Long mobile)
    {
        this.mobile = mobile;
    }

    public Date getDateOfBirth()
    {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth)
    {
        this.dateOfBirth = dateOfBirth;
    }

    public ArrayList<String> getSkills()
    {
        return skills;
    }

    public void setSkills(ArrayList<String> skills)
    {
        this.skills = skills;
    }
}
